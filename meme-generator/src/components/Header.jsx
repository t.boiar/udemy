import React from "react";

function Header() {
  return (
    <header className="header">
      <div className="header-logo">
        <img className="logo-img" src="public/assets/logo.svg" />
        <h1 className="logo-text">Meme Generator</h1>
      </div>
      <p className="header-title">React Course</p>
    </header>
  );
}

export default Header;
