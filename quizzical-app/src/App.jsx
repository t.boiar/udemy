import React from "react";
import StartPage from "./StartPage";
import QuizzPage from "./QuizzPage";
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" index element={<StartPage />} />
        <Route path="quizz" element={<QuizzPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
