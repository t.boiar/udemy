import React from "react";

export default function Score(props){
	return <span className="result">You scored {props.matches}/5 correct answers</span>
}