import React from "react";
import { Link } from "react-router-dom";

export default function StartPage() {
  return (
    <div className="content start-page">
      <h1 className="title">Quizzical</h1>
      <p className="descript">Some description if needed</p>
      <Link to="quizz" className="btn btn-quizz">
        Start Quizz
      </Link>
    </div>
  );
}
