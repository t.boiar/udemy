import React, { useState, useEffect } from "react";
import Question from "./Question";
import Score from "./Score";

export default function QuizzPage() {
  const [quizz, setQuizz] = useState([]);
	const [matches, setMatches] = useState(0);
	const [checkBtn, setCheckBtn] = useState("Check answers");
  const [results, setResults] = useState(false);

  useEffect(() => {
    fetch("https://opentdb.com/api.php?amount=5&category=9&difficulty=medium")
      .then((res) => res.json())
      .then((quizzData) => setQuizz(quizzData.results));
  }, []);

  function checkAnswers(e) {
		if(checkBtn === "Play again") {
			window.location.reload(false);
		}
    setCheckBtn((e.target.innerText = "Play again"));
    setResults(true);
  }

  const questionElements = quizz.map((question, index) => (
    <Question
		setMatches={setMatches}
      results={results}
      key={index}
      question={question.question}
      correct={question.correct_answer}
      incorrect={question.incorrect_answers}
    />
  ));

  let score;
  if (checkBtn === "Play again") {
    score = <Score matches={matches}/>;
  }


  return (
    <div className="content quizz-page">
      {questionElements}
      <div className="check-wrap">
        {score}
        <button className="btn btn-check" onClick={checkAnswers}>
          {checkBtn}
        </button>
      </div>
    </div>
  );
}

// 1. green - for correct answers
// 2. red - for incorrect answers
// 3. inactive - for others
// 4. calculate amount of correct answers
// 5. start a new game