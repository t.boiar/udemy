import React from "react";

function Navbar() {
  return (
    <nav className="nav">
      <div className="nav--logo">
        <img
          className="nav--logo_icon"
          src="src/images/reactjs-icon.png"
          alt="react-logo"
        />
        <h3 className="nav--logo_text">ReactFacts</h3>
      </div>
      <h4 className="nav--title">React Course - Project 1</h4>
    </nav>
  );
}

export default Navbar;
