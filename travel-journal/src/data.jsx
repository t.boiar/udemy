export default [
  {
    id: 0,
    title: "Kjeragbolten",
    location: "Norway",
    googleMapsUrl: "https://goo.gl/maps/NtymyQjXNV8tWpy7A",
    startDate: "23 Jun, 2018",
    endDate: "30 Jun, 2018",
    description:
      "Kjeragbolten is a boulder on the mountain Kjerag in Sandnes municipality in Rogaland county, Norway. The rock itself is a 5-cubic-metre  glacial deposit wedged in a large crevice in the mountain. It is suspended above a 984-metre deep abyss.",
    imageUrl: "../public/assets/card-imgs/kjerag.jpeg",
  },
  {
    id: 1,
    title: "Lysebotn",
    location: "Norway",
    googleMapsUrl: "https://goo.gl/maps/wYZmCfv4SrzqWJAk8",
    startDate: "23 Jun, 2018",
    endDate: "30 Jun, 2018",
    description:
      "Lysebotn is a village in Sandnes municipality in Rogaland county, Norway. The village is located at the eastern end of the Lysefjorden in a very isolated valley that is only accessible by one road or by boat. The name itself means the 'bottom [end] of the Lysefjorden'.",
    imageUrl: "../public/assets/card-imgs/lysebotn.jpg",
  },
  {
    id: 2,
    title: "Venice",
    location: "Italy",
    googleMapsUrl: "https://goo.gl/maps/5UyH8bLgU3rxeN22A",
    startDate: "29 Dec, 2018",
    endDate: "2 Jan, 2019",
    description:
      "Venice is a city in northeastern Italy and the capital of the Veneto region. It is built on a group of 118 small islands that are separated by canals and linked by over 400 bridges.",
    imageUrl: "../public/assets/card-imgs/venice.jpg",
  },
  {
    id: 3,
    title: "Lake Como",
    location: "Italy",
    googleMapsUrl: "https://goo.gl/maps/w4KGCCMDBDuRyhAr8",
    startDate: "29 Dec, 2018",
    endDate: "2 Jan, 2019",
    description:
      "Lake Como, also known as Lario, is a lake of glacial origin in Lombardy, Italy. It has an area of 146 square kilometres, making it the third-largest lake in Italy, after Lake Garda and Lake Maggiore. At over 400 metres deep, it is the fifth deepest lake in Europe. Located at the foot of the Alps, Lake Como provides one of the most picturesque sceneries in the world.",
    imageUrl: "../public/assets/card-imgs/como.jpg",
  },
  {
    id: 4,
    title: "Verona",
    location: "Italy",
    googleMapsUrl: "https://goo.gl/maps/8NJfcPBS3Jkec1vL8",
    startDate: "29 Dec, 2018",
    endDate: "2 Jan, 2019",
    description:
      'Verona is a city in northern Italy’s Veneto region, with a medieval old town built between the meandering Adige River. It’s famous for being the setting of Shakespeare’s "Romeo and Juliet." A 14th-century residence with a tiny balcony overlooking a courtyard is said be “Juliet’s House." The Verona Arena is a huge 1st-century Roman amphitheater, which currently hosts concerts and large-scale opera performances.',
    imageUrl: "../public/assets/card-imgs/verona.jpg",
  },
  {
    id: 5,
    title: "Mount Strymba",
    location: "Ukraine",
    googleMapsUrl: "https://goo.gl/maps/ddTzRoU1TMGcfZDW8",
    startDate: "8 Aug, 2020",
    endDate: "9 Aug, 2020",
    description:
      'Strymba - mountain peak in the Ukrainian Carpathians, in the Gorgany massif (Inner Gorgany). Located on the border between Mezhgorsky and Tyachiv districts of the Transcarpathian region, on the same ridge - Strymba. The southwestern slopes of the mountain lie within the limits of the National Natural Park "Synevir". Strymba\'s height is 1719 m. The top is triangular in shape, formed in fine-grained sandstones by deep catchment funnels. There are small lakes, stone placers.',
    imageUrl: "../public/assets/card-imgs/strymba.jpg",
  },
  {
    id: 6,
    title: "Lycian Way",
    location: "Turkey",
    googleMapsUrl: "https://goo.gl/maps/SGR4o9H4ehgMdYCg8",
    startDate: "15 Sep, 2020",
    endDate: "25 Sep, 2020",
    description:
      "The Lycian Way is a marked long-distance trail in southwestern Turkey around part of the coast of ancient Lycia. It is over 500 km (310 mi) in length and stretches from Hisarönü (Ovacık), near Fethiye, to Geyikbayırı in Konyaaltı about 20 km (12 mi) from Antalya. It is waymarked with red and white stripes of the Grande Randonnee convention. It was conceived by Briton Kate Clow, who lives in Turkey. It takes its name from the ancient civilization, which once ruled the area",
    imageUrl: "../public/assets/card-imgs/likyaYolu.jpg",
  },
  {
    id: 7,
    title: "Mount Khom'yak",
    location: "Ukraine",
    googleMapsUrl: "https://goo.gl/maps/DUpaTasbBCu9dQTKA",
    startDate: "29 May, 2021",
    endDate: "29 May, 2021",
    description:
      "Rocky, forested peak with trekking paths & a rope bridge, waterfalls & scenic panoramas. Mount Khomyak is part of the highest ridge of the Eastern Gorgan - Sinyak, which is located in the southeastern part of the mountain range. People connect the name of the mountain with the stay in this area marmot alpine (hamster). The height of the Mount Khomyak reaches 1542 meters",
    imageUrl: "../public/assets/card-imgs/khomyak.jpg",
  },
  {
    id: 8,
    title: "Mount Parashka",
    location: "Ukraine",
    googleMapsUrl: "https://goo.gl/maps/vGLoH76kKMdpVLLg7",
    startDate: "27 Jun, 2021",
    endDate: "27 Jun, 2021",
    description:
      "Parashka – the highest peak in the Skole Beskids. Highest peak reaches 1268,5 metres above sea level. It is located at a distance 8 kilometres north-west from the district center Skole and 118 kilometres from Lviv.",
    imageUrl: "../public/assets/card-imgs/parashka.jpg",
  },
  {
    id: 9,
    title: "Polonyna Yavirnyk",
    location: "Ukraine",
    googleMapsUrl: "https://goo.gl/maps/SD7HGU6XWSTD8BvXA",
    startDate: "16 Aug, 2021",
    endDate: "17 Aug, 2021",
    description:
      "Yavirnyk is a picturesque massif near the city of Yaremche, Ivano-Frankivsk region. From it you can see the peaks of Khomyak and Sinyak as if in the palm of your hand, and in clear weather you can see all of Montenegro. At an altitude of more than 1,200 m above sea level, houses have been built in the Yavirnyk valley where travelers can spend the night.",
    imageUrl: "../public/assets/card-imgs/yavirnyk.jpg",
  },
];
