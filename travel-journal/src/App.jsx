import React from "react";
import Navbar from "./components/Navbar";
import Card from "./components/Card";
import cardsData from "./data";

function App() {
  const cards = cardsData.map((card) => {
    return <Card key={card.id} {...card} />;
  });

  return (
    <div>
      <Navbar />
      <div className="cards-list">{cards}</div>
    </div>
  );
}

export default App;
